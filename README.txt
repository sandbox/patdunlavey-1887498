DRUPAL SPLASHIFY DATE RANGE MODULE
------------------------
Maintainers:
  Pat Dunlavey (http://drupal.org/user/5427)

Requires - 
- Drupal 7
- Splashify 7.x-1.x-dev with the patch that enables hook_splashify_config_alter, 
  see http://drupal.org/node/1886896
- Splashify's dependencies, see http://drupal.org/project/splashify)

License - GPL (see LICENSE)


1.0 OVERVIEW
-------------
Splashify Date Range is an add-on module to Splashify that permits a time window
to be specified for the splash screen to appear. The splash screen will only appear
inside that time window.

2.0 INSTALLATION
-----------------
1. Download and enable the Splashify module: http://drupal.org/project/splashify
2. Enable this module 


2.1 CONFIGURATION
------------------
Go to "Configuration" -> "System" -> "Splashify" to find all the configuration
options. Under the "When" tab, a new "Dates" fieldset will appear on the configuration
form. Enabling (check the "Restrict Dates" checkbox) and setting start and end dates/times will
restrict the dates that the splash screen will appear to users.

LAST UPDATED
-------------
01/13/2013